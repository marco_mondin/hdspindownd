/*
Copyright 2016 Mondin Marco (Prometheus Technologies)

This file is part of hdspindownd.

hdspindownd is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdspindownd is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdspindownd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QTimer>
#include <QStringList>
#include <QFile>
#include <QRegularExpression>
#include <QProcess>

class Manager : public QObject
{
    Q_OBJECT
public:
    enum fields{ Unused,
                 MajorNumber,
                 MinorNumber,
                 DeviceName,
                 ReadsCompletedSuccessfully,
                 ReadsMerged,
                 SectorsRead,
                 TimeSpentReading,
                 WritesCompleted,
                 WritesMerged,
                 SectorsWritten,
                 TimeSpentWriting,
                 IOsCurrentlyInProgress,
                 TimeSpentDoingIOs,
                 WeightedTimeSpentDoingIOs,
                 SizeOfFields };

    explicit Manager(const QStringList &args, QObject *parent = 0);

signals:

public slots:
    void eventloopStarted();

protected slots:
    void check();

private:
    const QStringList args;
    QFile *deviceStats;
    QString device;
    QString realDevice;
    bool verboseMode;
    int defaultTimeout;
    QTimer *timer;
    const QRegularExpression splitter;
    QStringList values;
    QStringList oldValues;
    QProcess *sgProcess;
};

#endif // MANAGER_H
