QT += core
QT -= gui

CONFIG += c++11

target.path = /usr/local/bin
INSTALLS += target

TARGET = hdspindownd
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    manager.cpp

HEADERS += \
    manager.h
