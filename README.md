This is a little daemon tested on rPi NAS for spindown external USB HD.
It use libqt5core and sg3_utils. 
This is a software daeomon to bypass many spindown problems with external USB units.
Many unit not support standard spindown paramater, but they spindown with sg_start.
This tool monitor HD activity via /proc/drivestats and spindown after a while of inactivity.
Example :
# hdspindownd -t=300 /dev/sda

Check every 5 minutes disk activity, and if disk result not used in this time it should spindown.

In raspbian a systemd unit can be created in "/etc/systemd/system/hdspindown.service" like this: 


```
#!python

[Unit]
Description=SpinDown HD after inactivity time

[Service]
ExecStart=/usr/local/bin/hdspindownd -t=300 /dev/sda
ExecStop=/usr/bin/killall hdspindownd
Type=forking
Restart=always

[Install]
WantedBy=default.target

```