/*
Copyright 2016 Mondin Marco (Prometheus Technologies)

This file is part of hdspindownd.

hdspindownd is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdspindownd is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdspindownd.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QCoreApplication>
#include <QTimer>
#include "manager.h"
#include <unistd.h>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    pid_t pid;
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
        qDebug()<<"Not forked!";
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }
    qDebug()<<"Daemon started!";

    QStringList *args = new QStringList;
    for(int index = 1; index < argc; index++) {
        args->append(QString(argv[index]));
        qDebug()<<args;
    }
    Manager m(*args);
    delete args;
    QTimer::singleShot(0, &m, SLOT(eventloopStarted()));

    return a.exec();
}
