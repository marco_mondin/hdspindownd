/*
Copyright 2016 Mondin Marco (Prometheus Technologies)

This file is part of hdspindownd.

hdspindownd is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdspindownd is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdspindownd.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "manager.h"
#include <QDebug>
#include <QCoreApplication>

Manager::Manager(const QStringList &args, QObject *parent) : QObject(parent),
    args(args),
    deviceStats(NULL),
    verboseMode(false),
    defaultTimeout(120),
    splitter("[ X]+")
{
    foreach (const QString & arg, args) {
        if(arg.size() > 0)
            if(arg.at(0) != '-' && device.isEmpty()) {
                realDevice = device = arg;
                device = device.split('/').last();
                device.prepend(" ");
                device.append(" ");
            }
        if(arg == "-v")
            verboseMode = true;
        if(arg.size() > 3)
            if(arg.left(3) == "-t=") {
                bool ok = false;
                int value = arg.right(arg.size() - 3).toInt(&ok);
                if(!ok || value < 10) {
                    qDebug()<<"Warning! minimum time = 10s! Forced to 10s!";
                    defaultTimeout = 10;
                } else {
                    defaultTimeout = value;
                }
            }
    }
}

void Manager::eventloopStarted()
{
    if(device.isEmpty()) {
        qDebug()<<"hdspindown by Mondin Marco (c)2016";
        qDebug()<<"\nUSAGE:\nhdspindown [-v] [t=1234] <device>";
        qDebug()<<"\n -v   verbose mode";
        qDebug()<<" -t=1234   timeout interval check";
        qDebug()<<" <device>   device path, example /dev/sda";
        qApp->quit();
    } else {
        deviceStats = new QFile("/proc/diskstats", this);
        if(!deviceStats->open(QFile::ReadOnly)) {
            qDebug()<<"Cannot access /proc/diskstats";
            qApp->quit();
            return;
        }
        timer = new QTimer(this);
        timer->setSingleShot(true);
        connect(timer, SIGNAL(timeout()), this, SLOT(check()));
        sgProcess = new QProcess(this);
        sgProcess->setProgram("sg_start");
        if(verboseMode) {
            sgProcess->setArguments(QStringList()<<"-v"<<"0"<<"--pc=2"<<realDevice);
        } else {
            sgProcess->setArguments(QStringList()<<"0"<<"--pc=2"<<realDevice);
        }
        if(verboseMode)
            qDebug()<<"verbose mode actived!";
        check();
    }
}

void Manager::check()
{
    deviceStats->seek(0);
    QString data(QString::fromUtf8(deviceStats->readAll()));
    QStringList rows = data.split("\n");
    foreach (const QString &row, rows) {
        if(row.contains(device)) {
            oldValues = values;
            values = row.split(splitter);
            if(values.size() >= SizeOfFields && oldValues.size() >= SizeOfFields) {
                int ammountWrite = values.at(SectorsWritten).toInt() -
                        oldValues.at(SectorsWritten).toInt();
                int ammountRead = values.at(SectorsRead).toInt() -
                        oldValues.at(SectorsRead).toInt();
                int currentIO = values.at(IOsCurrentlyInProgress).toInt();
                if (verboseMode) {
                    qDebug()<<"Bytes in last interval :";
                    qDebug()<<"Readed : "<<ammountRead<<"   Written : "<<ammountWrite<<"   Current IO : "<<currentIO;
                }
                if ((ammountRead + ammountWrite + currentIO) == 0) {
                    if(verboseMode) {
                        qDebug()<<"Inactive! Spinning Down";
                        sgProcess->setProcessChannelMode(QProcess::MergedChannels);
                    }
                    sgProcess->start();
                    sgProcess->waitForFinished();
                    if(verboseMode) {
                        sgProcess->waitForReadyRead();
                        qDebug()<<sgProcess->readAll();
                    }
                } else {
                    if(verboseMode)
                        qDebug()<<"Disk active! Not spinning down";
                }
            }
        }
    }
    timer->start(defaultTimeout * 1000);
}
